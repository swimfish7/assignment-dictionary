section .text

%define BUFFER_SIZE 256
%define KEY_SHIFT 8
%include "lib.inc"
%define newline 0xA
extern find_word
global _start

section .data

buffer_overflow: db "The key length is more than 255",   newline, 0     ; messages
key_not_found:   db "The key isn't exist in dictionary", newline, 0

%include "words.inc"

section .text
    _start:
        sub rsp, BUFFER_SIZE                                            ; allocate memory in stack
        mov rdi, rsp                                                    ; set args for read_word call
        mov rsi, BUFFER_SIZE
        call read_word                                                  ; returns 0 if we can't read word
        push rdx                                                        ; save key length
        je .buffer_overflow_exc
        mov rdi, rax                                                    ; set args for find_word call
        mov rsi, CURRENT_START
        call find_word
        cmp rax, 0
        je .key_not_found_exc
        add rax, KEY_SHIFT                                              ; to get value of key, we add KEY_SHIFT
        pop rdx
        add rax, rdx                                                    ; and key length
        inc rax                                                         ; inc because of added null terminator
        mov rdi, rax                                                    ; set arg for value printing
        jmp .ok
    .buffer_overflow_exc:
        mov rdi, buffer_overflow                                        ; set arg for error message printing
        call print_string_err
        mov rdi, 1                                                      ; returned error code
        jmp .end
    .key_not_found_exc:
        mov rdi, key_not_found                                          ; set arg for error message printing
        call print_string_err
        mov rdi, 1                                                      ; returned error code
        jmp .end
    .ok:
        call print_string                                               ; print value from key
    .end:
        add rsp, BUFFER_SIZE                                            ; restore stack
        call exit
